package com.bilas.generics;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

public class PriorityQueue<Droid> {

    private static final int DEFAULT_DEQUEUE_SIZE = 15;
    private Object[] droids = new Object[DEFAULT_DEQUEUE_SIZE];
    private int size = 0;
    private final Comparator<? super Droid> comparator;

    public PriorityQueue(Comparator<? super Droid> comparator) {
        this.comparator = comparator;
        this.droids = new Object[DEFAULT_DEQUEUE_SIZE];
    }

    public PriorityQueue(int initialCapacity,
                         Comparator<? super Droid> comparator) {
        if (initialCapacity < 1)
            throw new IllegalArgumentException();
        this.droids = new Object[initialCapacity];
        this.comparator = comparator;
    }

    public PriorityQueue(int initialCapacity) {
        this(initialCapacity, null);
    }

    public PriorityQueue() {
        this.comparator = new Comparator<Droid>() {
            @Override
            public int compare(Droid o1, Droid o2) {
                if (o1.equals(o2)){
                    return 0;
                }
                return -1;
            }

        };
    }


    public int size() {
        return size;
    }

    public boolean isEmpty() {
        int count = 0;
        for (Object o : droids) {
            if (o != null){
                count++;
            }
        }
        return count == 0;
    }

    public void add(Droid droid) {
        if (droid == null) {
            throw new NullPointerException();
        }
        if(size == droids.length-1)
            changeElementsSize(droids.length*2);
        droids[size++] = droid;
    }



    private void changeElementsSize(int newLength){
        Object[] newArray = new Object[newLength];
        System.arraycopy(droids, 0, newArray, 0, size);
        droids = newArray;
    }

    public boolean remove(Object o) {
        for (int i = 0; i < droids.length; i++) {
            if (o.equals(droids[i])){
                droids[i] = null;
                return true;
            }
        }
        return false;
    }

    public void clear() {
        for (int i = 0; i < size; i++)
            droids[i] = null;
        size = 0;
    }

    public Object poll() {
        if (size == 0) {
            return null;
        }
        int s = --size;
        Droid result = (Droid) droids[0];
        Droid x = (Droid) droids[s];
        droids[s] = null;
        return result;
    }

    public Object peek() {
        if(size == 0) {
            return null;
        } else {
            return (Droid) droids[0];
        }
    }

    public boolean contains(Object o) {
        boolean boo = false;
        for (Object ob : droids) {
            if (o.equals(ob)){
                boo = true;
            }
        }
        return boo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PriorityQueue<?> that = (PriorityQueue<?>) o;
        return size == that.size &&
                Arrays.equals(droids, that.droids) &&
                Objects.equals(comparator, that.comparator);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(size, comparator);
        result = 31 * result + Arrays.hashCode(droids);
        return result;
    }

    @Override
    public String toString() {
        return "PriorityQueue{" +
                "droids=" + Arrays.toString(droids) +
                ", size=" + size +
                ", comparator=" + comparator +
                '}';
    }
}
