package com.bilas.generics;

public class Main {
    public static void main(String[] args) {
//        Ship<Droid> ship = new Ship();
        Ship ship = new Ship<>();

        ship.putDroidToShip(new Man("igor"));
        ship.putDroidToShip(new Droid());

        String s = ship.toString();
        System.out.println(s);

        PriorityQueue<Droid> priorityQueue = new PriorityQueue<Droid>();

        priorityQueue.add(new Droid());
        priorityQueue.add(new Droid());

        for (int i = 0; i < priorityQueue.size(); i++) {
            System.out.println(priorityQueue);
        }


    }
}
