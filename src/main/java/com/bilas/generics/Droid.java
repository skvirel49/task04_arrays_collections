package com.bilas.generics;

import java.util.Objects;

public class Droid {
    private int id;
    public static int count = 0;

    public Droid() {
        this.id = count++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Droid droid = (Droid) o;
        return id == droid.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Droid{" +
                "id=" + id +
                '}';
    }
}
