package com.bilas.generics;

import java.util.LinkedList;
import java.util.List;

public class Ship<Droid> {
    private List<Droid> dList;

    public Ship() {
        this.dList = new LinkedList<>();
    }

    public void putDroidToShip(Droid d){
        dList.add(d);
    }

    public Droid getDroidFromShip(Droid d){
        return dList.get(dList.indexOf(d));
    }

    @Override
    public String
    toString() {
        return "Ship{" +
                "dList=" + dList +
                '}';
    }
}
