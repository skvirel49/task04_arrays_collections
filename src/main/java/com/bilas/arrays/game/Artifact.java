package com.bilas.arrays.game;

import java.util.Objects;

public class Artifact {
    private int strength;

    Artifact() {
        this.strength = (int) ((Math.random() * 70) + 10);
    }

    int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Artifact artifact = (Artifact) o;
        return strength == artifact.strength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength);
    }

    @Override
    public String toString() {
        return "Artifact{" +
                "strength=" + strength +
                '}';
    }
}
