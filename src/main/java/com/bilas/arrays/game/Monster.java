package com.bilas.arrays.game;

import java.util.Objects;

public class Monster {
    private int strength;

    Monster() {
        strength = (int) ((Math.random() * 95) + 5);
    }

    int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Monster monster = (Monster) o;
        return strength == monster.strength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength);
    }

    @Override
    public String toString() {
        return "Monster{" +
                "strength=" + strength +
                '}';
    }
}
