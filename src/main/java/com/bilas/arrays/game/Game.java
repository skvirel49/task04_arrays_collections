package com.bilas.arrays.game;

public class Game implements Model {

    private Hero hero;

    Game() {
        this.hero = new Hero();
    }

    @Override
    public void play() {
        hero.openDoor();
    }
}
