package com.bilas.arrays.game;

import java.util.Objects;
import java.util.Scanner;

public class Hero {
    private final int HERO_STRENGTH = 25;
    private int strength;
    private Door door;
    private int point = 0;
    private Scanner scanner = new Scanner(System.in);

    Hero() {
        this.strength = HERO_STRENGTH;
        door = new Door();
    }

    public int getHERO_STRENGTH() {
        return HERO_STRENGTH;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hero hero = (Hero) o;
        return strength == hero.strength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(HERO_STRENGTH, strength);
    }

    @Override
    public String toString() {
        return "Hero{" +
                "strength=" + strength +
                '}';
    }

    void openDoor(){
        System.out.println("add number of door:");
        int num = scanner.nextInt();
        if (door.getDoors()[num] instanceof Monster){
            Monster monster = (Monster) door.getDoors()[num];
            if (strength >= monster.getStrength()){
                System.out.println("Yoo win");
                point++;
            } else {
                System.out.println("You loose");
            }
        }
        if (door.getDoors()[num] instanceof Artifact){
            Artifact artifact = (Artifact) door.getDoors()[num];
            strength += artifact.getStrength();
            System.out.println("you found artifact. Your strength is " + strength);
        }
    }
}
