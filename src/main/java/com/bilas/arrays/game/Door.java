package com.bilas.arrays.game;

import java.util.Arrays;
import java.util.Objects;

public class Door<E> {

    private final int DOORS_COUNT = 10;
    private E[] doors = (E[]) new Object[DOORS_COUNT];

    Door() {
        for (int i = 0; i < doors.length; i++) {
            int random = (int) (Math.random() * 10);
            if (random < 5){
                doors[i] = (E) new Monster();
            } else {
                doors[i] = (E) new Artifact();
            }
        }
    }

    Object[] getDoors() {
        return doors;
    }

    public void setDoors(Object[] doors) {
        this.doors = (E[]) doors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Door<?> door = (Door<?>) o;
        return Arrays.equals(doors, door.doors);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(DOORS_COUNT);
        result = 31 * result + Arrays.hashCode(doors);
        return result;
    }

    @Override
    public String toString() {
        return "Door{" +
                "DOORS_COUNT=" + DOORS_COUNT +
                ", doors=" + Arrays.toString(doors) +
                '}';
    }
}
