package com.bilas.arrays.game;

import java.util.Scanner;

class MyView {
    private Game game;
    private Scanner scanner = new Scanner(System.in);

    void print(){
        game = new Game();
        do {
            outputMenu();
            int num = scanner.nextInt();
            switcher(num);
        } while (true);
    }

    private static void outputMenu() {
        System.out.println("\nMENU:");
        System.out.println("1 - play");;
        System.out.println("0 - to exit");
    }


    private void switcher(int num){
        switch (num){
            case 1:
                pressButton1();
                break;
            case 0:
                System.exit(0);
                break;
            default:
                System.out.println("Wrong key!");
                break;
        }
    }

    private void pressButton1(){
        game.play();
    }
}
