package com.bilas.arrays.logic;

class MyArray {

    int[] newArrayFromSameElements(int[] array1, int[] array2) {
        int size = 0;
        for (int item : array1) {
            for (int value : array2) {
                if (item == value) {
                    size++;
                }
            }
        }
        int[] array = new int[size];
        int k = 0;
        for (int item : array1) {
            for (int value : array2) {
                if (item == value) {
                    array[k] = item;
                    k++;
                }
            }
        }
        return array;
    }


}
