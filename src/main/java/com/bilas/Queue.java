package com.bilas;

public class Queue {
    private char[] queue;
    private int putLoc;
    private int getLoc;
    private int size;

    public Queue() {
        queue = new char[15];
        this.putLoc = 0;
        this.getLoc = 0;
    }

    public Queue(int size) {
        this.queue = new char[size + 1];
        this.putLoc = 0;
        this.getLoc = 0;
    }

    public char[] getQueue() {
        return queue;
    }

    public void setQueue(char[] queue) {
        this.queue = queue;
    }

    public int getPutLoc() {
        return putLoc;
    }

    public void setPutLoc(int putLoc) {
        this.putLoc = putLoc;
    }

    public int getGetLoc() {
        return getLoc;
    }

    public void setGetLoc(int getLoc) {
        this.getLoc = getLoc;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void put(char ch){
        if (putLoc == queue.length - 1){
            enlargeQueue();
        }
        queue[putLoc] = ch;
        putLoc++;
    }

    char get(){
        if (getLoc == putLoc){
            return 0;
        }
        getLoc++;
        return queue[getLoc - 1];
    }

    private void enlargeQueue() {
        char[] tempQ = queue;
        queue = new char[size = size * 2];
        for (int i = 0; i < tempQ.length; i++) {
            putLoc++;
            queue[i] = tempQ[i];
        }
    }
}
